#!/bin/bash
# APP
ID=`docker run -d -p 8000 --name app mmitrofan/crossoverdevops:latest`
echo $ID
PORT=`docker inspect $ID | python -c "import sys,json;j=json.loads(sys.stdin.read()); print j[0]['NetworkSettings']['Ports']['80/tcp'][0]['HostPort']"`
sleep 3
open http://localhost:$PORT
