from __future__ import unicode_literals

SECRET_KEY = "%(secret_key)s"
NEVERCACHE_KEY = "%(nevercache_key)s"

DATABASES = {
    "default": {
        # Ends with "postgresql_psycopg2", "mysql", "sqlite3" or "oracle".
        "ENGINE": "django.db.backends.mysql",
        # DB name or path to database file if using sqlite3.
        "NAME": "crossovermysql",
        # Not used with sqlite3.
        "USER": "crossovermysql",
        # Not used with sqlite3.
        "PASSWORD": "crossovermysql",
        # Set to empty string for localhost. Not used with sqlite3.
        "HOST": "crossovermysql.cukywharz7io.us-east-1.rds.amazonaws.com",
        # Set to empty string for default. Not used with sqlite3.
        "PORT": "3306",
    }
}

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTOCOL", "https")

CACHE_MIDDLEWARE_SECONDS = 60

CACHE_MIDDLEWARE_KEY_PREFIX = "%(proj_name)s"

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.memcached.MemcachedCache",
        "LOCATION": "127.0.0.1:11211",
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
